module SIMPLEB(
input clock,
input reset,
input exec,
input[15:0] exInput,
output reg [15:0] exOutput,
output selecter,

output debug,
output [15:0] debug0,debug1,debug2,debug3,debug4,
output[2:0] debug5,
output [15:0] debug6,debug7
);

reg [127:0] register;
// pipeline register //
// IF/ID //
	reg [15:0] ifidIR;
// ID/EX //
	reg [1:0] idexop1;
	reg [2:0] idexop2, idexop3;
	reg [3:0] idexop4, idexop5;
	reg [15:0] idexAR, idexBR;
// EX/MEM //
	reg [1:0] exmemop1;
	reg [2:0] exmemop2, exmemop3;
	reg [3:0] exmemop4;
	reg [15:0] exmemDR;
// MEM/WD //
	reg [1:0] memwdop1;
	reg [2:0] memwdop2, memwdop3;
	reg [3:0] memwdop4;
	reg [15:0] memwdDR, memwdMDR;
	

//wire between IF/ID-ID/EX //
wire [1:0] op1_to_idex;
wire [2:0] op2_to_idex, op3_to_idex;
wire [3:0] op4_to_idex, op5_to_idex;
//wire between ID/EX-EX/MEM //
wire [1:0] op1_to_exmem;
wire [2:0] op2_to_exmem, op3_to_exmem;
wire [3:0] op4_to_exmem, op5_to_exmem;
//wire between EX/MEM-MEM/WD //
wire [1:0] op1_to_memwd;
wire [2:0] op2_to_memwd, op3_to_memwd;
wire [3:0] op4_to_memwd;
//wire between MEM/WD-p5 //
wire [1:0] op1_to_p5;
wire [2:0] op2_to_p5, op3_to_p5;
wire [3:0] op4_to_p5;


//control SIMPLEB //

// control p1 //
reg fetch;
reg startp1;

// control p2 //
reg p2clock;

// control p4 //
reg mem_clock;

//control memory//
reg write_enable = 0;
wire clock_mem;
wire [15:0] q;
wire [15:0] address;
wire [15:0] data_to_memory;

//connection///
//p1
wire p1wren;
wire [15:0] p1_data, p1_memory, p1IR, pc;

//p2
wire p2wren;
wire [2:0] p2reg;
wire [15:0] p2IR, p2AR, p2BR, reg001, reg010, p2_data;
//p3
wire [3:0] code;
wire [15:0] p3AR, p3BR, p3DR;
wire Ofrag;
wire [16:0] Output;

//p4
wire [15:0] p4_memory, DR_I, MDR, DR_O, p4MDR;
//p5
wire [15:0] p5DR, p5MDR;
//p


reg [2:0]phase_counter; //000 = phase1, 001 = phase2.....


//connect phase modelu///
p1 phase1(.set_p5address(p1wren), .fetch(fetch), .start(startp1), .reset(ch_reset),
				.p5(p1_data),.data_path(p1_memory),.IR(p1IR),.pc(pc));
				
p2 phase2( .IR(p2IR),.regs(register),.AR(p2AR),.BR(p2BR),
				.opc1(op1_to_idex), .opc2(op2_to_idex), .opc3(op3_to_idex), 
				.opc4(op4_to_idex), .opc5(op5_to_idex));
				
p3 phase3(.AR(p3AR), .BR(p3BR),.op1(op1_to_exmem), .op2(op2_to_exmem), 
            .op3(op3_to_exmem), .op4(op4_to_exmem), .op5(op5_to_exmem), 
				.exOutFrag(Ofrag), .DR(p3DR), .out(Output), .code(code));
				
p4 phase4(.in(exInput), .data_path(p4_memory), .op1(op1_to_memwd), .op4(op4_to_memwd), 
				.DR_I(DR_I), .MDR(p4MDR), .DR_O(DR_O));

p5 phase5(.DR(p5DR), .MDR(p5MDR), .op1(op1_to_p5), .op2(op2_to_p5), 
            .op3(op3_to_p5), .op4(op4_to_p5), .out1(p1_data), .out2(p2_data), 
				.p5adress1(p1wren), .p5adress2(p2wren), .address(p2reg));

//connect memory
memory memory(.address(address),.clock(clock_mem),.data(data_to_memory),.wren(write_enable),.q(q));

//chattering
wire ch_reset;
wire ch_exec;
chattering chreset(.clk(clock), .in(reset), .out(ch_reset));  
chattering chexec(.clk(clock), .in(exec), .out(ch_exec));


function[15:0] mux;
input [15:0] in1, in2;
input [2:0] phase;
begin
	case(phase)
	3'b000 : mux = in1;   //phase1
	3'b001 : mux = in1;
	3'b010 : mux = in2;   //phase4
	3'b011 : mux = in2;
	3'b100 : mux = in2;
	default : mux = 16'b0000_0000_0000_0000;
	endcase
end
endfunction

initial begin
	phase_counter <= 3'b000;
end

always @(posedge ~clock) begin

if(ch_exec != 1 && Output[16] != 1)begin
//////////phase1.5/////////////////
	if(phase_counter == 3'b001)begin
	fetch <= 1;
/////////phase3.5//////////////////
	end if(phase_counter == 3'b010)begin
	if(q[15:14] == 2'b01)begin  //store
	write_enable <= 1; 
	end else begin              //load
	write_enable <= 0;
	end
//////////phase5.5//////////////
	end if(phase_counter == 3'b000)begin
	if(p2wren == 1) begin
	case(p2reg)
	   3'b000: register[127:112] <= p2_data;
		3'b001: register[111:96]  <= p2_data;
		3'b010: register[95:80] <= p2_data;
		3'b011: register[79:64] <= p2_data;
		3'b100: register[63:48] <= p2_data;
		3'b101: register[47:32] <= p2_data;
		3'b110: register[31:16] <= p2_data;
		3'b111: register[15:0] <= p2_data;
		default : register = 0;
	 endcase
	end
	end
end
end

always @(posedge clock) begin 
if(reset == 1) begin
	phase_counter <= 3'b000;
end else if(ch_exec != 1 && Output[16] != 1)begin
//////////phase1///////////////////
	if(phase_counter == 3'b000)begin
	startp1 <= 1;
	phase_counter <= phase_counter + 3'b001;
//////////phase2///////////////////
   end if(phase_counter == 3'b001)begin
	startp1 <= 0;
	ifidIR <= p1IR;
   phase_counter <= phase_counter + 3'b001;
//////////phase3///////////////////
   end if(phase_counter == 3'b010)begin
	idexAR <= p2AR;
	idexBR <= p2BR;
	idexop1 <= op1_to_idex;
	idexop2 <= op2_to_idex;
	idexop3 <= op3_to_idex;
	idexop4 <= op4_to_idex;
	idexop5 <= op5_to_idex;
	phase_counter <= phase_counter + 3'b001;
//////////phase4///////////////////
   end if(phase_counter == 3'b011)begin
	exmemDR <= p3DR;
	exmemop1 <= op1_to_exmem;
	exmemop2 <= op2_to_exmem;
	exmemop3 <= op3_to_exmem;
	exmemop4 <= op4_to_exmem;
   phase_counter <= phase_counter + 3'b001;
	if(Ofrag == 1) exOutput <= Output[15:0];
//////////phase5///////////////////
   end if(phase_counter == 3'b100)begin
	memwdDR <= DR_O;
	memwdMDR <= p4MDR;
	memwdop1 <= op1_to_memwd;
	memwdop2 <= op2_to_memwd;
	memwdop3 <= op3_to_memwd;
	memwdop4 <= op4_to_memwd;
   phase_counter <= 3'b000;
	end
end
end

assign address = mux(pc, p3DR, phase_counter);
assign clock_mem = ~clock;

// phase1.5 //
	assign p1_memory = q;
// phase2 //
	assign p2IR = ifidIR;
// phase3 //
	assign p3AR = idexAR;
	assign p3BR = idexBR;
	assign op1_to_exmem = idexop1;
	assign op2_to_exmem = idexop2;
	assign op3_to_exmem = idexop3;
	assign op4_to_exmem = idexop4;
	assign op5_to_exmem = idexop5;
// phase3.5 //
	assign data_to_memory = p3BR;
// phase4 //
	assign op1_to_memwd = exmemop1;
	assign op2_to_memwd = exmemop2;
	assign op3_to_memwd = exmemop3;
	assign op4_to_memwd = exmemop4;
	assign DR_I = exmemDR;
// phase5 //
	assign p5DR = memwdDR;
	assign p5MDR = memwdMDR;
	assign op1_to_p5 = memwdop1;
	assign op2_to_p5 = memwdop2;
	assign op3_to_p5 = memwdop3;
	assign op4_to_p5 = memwdop4;
	
assign selecter = 1;

assign debug = Output[16];
assign debug0 = pc;
assign debug1 = q;
assign debug2 = p3AR;
assign debug3 = p3BR;
assign debug4 = p3DR;
assign debug5 = phase_counter;
assign debug6 = reg001;
assign debug7 = reg010;

endmodule
