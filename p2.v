module p2(
input [15:0] IR, //MARK: check err can reg is used in input?
input [127:0] regs,
output reg[15:0] AR,
output reg[15:0] BR,
output [1:0] opc1,
output [2:0] opc2,opc3,
output [3:0] opc4,opc5
);

parameter opADD = 2'b11,opSTORE = 2'b01,opLD = 2'b00,opCB = 2'b10; //op1

wire [1:0] op1;
wire [3:0] op3;
wire [2:0] Ra,Rb,Rs,Rd;
//reg [127:0] regs; //16*8 


assign op1[1:0] = IR[15:14]; 

assign Ra[2:0] = IR[13:11];
assign Rb[2:0] = IR[10:8];
assign Rs[2:0] = IR[13:11];
assign Rd[2:0] = IR[10:8];

assign opc1 = op1;
assign opc2 = IR[13:11];
assign opc3 = IR[10:8];
assign opc4 = IR[7:4];
assign opc5 = IR[3:0];

assign reg001 = regs[111:96];
assign reg010 = regs[95:80];
//////////////////decode IR///////////////////////////////////

//////////////////////////////////////////////////////////////////
////////////////////////set data//////////////////////////////////

/*always @(posedge wreg) begin
  if (setDataToRegs == 1 && reset != 1)begin
  case(address)
	   3'b000: regs[127:112] <= fromp5;
		3'b001: regs[111:96]  <= fromp5;
		3'b010: regs[95:80] <= fromp5;
		3'b011: regs[79:64] <= fromp5;
		3'b100: regs[63:48] <= fromp5;
		3'b101: regs[47:32] <= fromp5;
		3'b110: regs[31:16] <= fromp5;
		3'b111: regs[15:0] <= fromp5;
		default : regs <= 0;
	 endcase
	end else if(reset == 1) begin
		regs <= 0;
	end
end	
*/
////////////loard/store operation/////////////////////
always @* begin  
  if(op1 == opLD||op1 == opSTORE)begin
  
    case(Ra)
	   3'b000: AR[15:0] <= regs[127:112];
		3'b001: AR[15:0] <= regs[111:96];
		3'b010: AR[15:0] <= regs[95:80];
		3'b011: AR[15:0] <= regs[79:64];
		3'b100: AR[15:0] <= regs[63:48];
		3'b101: AR[15:0] <= regs[47:32];
		3'b110: AR[15:0] <= regs[31:16];
		3'b111: AR[15:0] <= regs[15:0];
		default : AR <= 0;
	 endcase
	 case(Rb)
      3'b000: BR[15:0] <= regs[127:112];
	   3'b001: BR[15:0] <= regs[111:96];
	   3'b010: BR[15:0] <= regs[95:80];
	   3'b011: BR[15:0] <= regs[79:64];
	   3'b100: BR[15:0] <= regs[63:48];
	   3'b101: BR[15:0] <= regs[47:32];
	   3'b110: BR[15:0] <= regs[31:16];
	   3'b111: BR[15:0] <= regs[15:0];
		default : BR <= 0;
    endcase
	 
/////////////////calc operation////////////////////////////	 
  
  end else if (op1 == opADD)begin
    case(Rs)
      3'b000: BR[15:0] <= regs[127:112];
	   3'b001: BR[15:0] <= regs[111:96];
	   3'b010: BR[15:0] <= regs[95:80];
	   3'b011: BR[15:0] <= regs[79:64];
	   3'b100: BR[15:0] <= regs[63:48];
	   3'b101: BR[15:0] <= regs[47:32];
	   3'b110: BR[15:0] <= regs[31:16];
	   3'b111: BR[15:0] <= regs[15:0];
		default : BR <= 0;
    endcase
	 case(Rd)
	   3'b000: AR[15:0] <= regs[127:112];
	   3'b001: AR[15:0] <= regs[111:96];
	   3'b010: AR[15:0] <= regs[95:80];
	   3'b011: AR[15:0] <= regs[79:64];
	   3'b100: AR[15:0] <= regs[63:48];
	   3'b101: AR[15:0] <= regs[47:32];
	   3'b110: AR[15:0] <= regs[31:16];
	   3'b111: AR[15:0] <= regs[15:0];
		default : AR <= 0;
	 endcase

///////////////conditional branch operation////////////////////////////////
    
	end else if (op1 == opCB)begin
	AR <= 0;
	case(Rb)
      3'b000: BR[15:0] <= regs[127:112];
	   3'b001: BR[15:0] <= regs[111:96];
	   3'b010: BR[15:0] <= regs[95:80];
	   3'b011: BR[15:0] <= regs[79:64];
	   3'b100: BR[15:0] <= regs[63:48];
	   3'b101: BR[15:0] <= regs[47:32];
	   3'b110: BR[15:0] <= regs[31:16];
	   3'b111: BR[15:0] <= regs[15:0];
		default : BR <= 0;
   endcase
	end else begin
		AR <= 0;
		BR <= 0;
	end
		
end

endmodule
