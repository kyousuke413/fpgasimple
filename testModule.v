

module testModule( 
	input clock, 
	input reset, 
	input exec, 
	input[15:0] exInput, 
	output[15:0] exOutput, 
	output selecterleg, 
	output [7:0] dataA, 
	output [7:0] dataB, 
	output [7:0] dataC, 
	output [7:0] dataD, 
	output [7:0] DRA, 
	output [7:0] DRB, 
	output [7:0] DRC, 
	output [7:0] DRD 
); 
	assign selecterleg = 1; 
	
	//wire 
	wire [15:0]IR; 
	wire [15:0]DR; 
	
	//outputleg DR 
	outputleg leg_A (.a(exOutput[15:12]), .selecter(), .out(dataA[7:0])); 
	outputleg leg_B (.a(exOutput[11:8]), .selecter(), .out(dataB[7:0])); 
	outputleg leg_C (.a(exOutput[7:4]), .selecter(), .out(dataC[7:0])); 
	outputleg leg_D (.a(exOutput[3:0]), .selecter(), .out(dataD[7:0])); 
	
	//outputleg IR00 
	outputleg  pcA(.a(DR[15:12]), .selecter(), .out(DRA[7:0])); 
	outputleg  pcB(.a(DR[11:8]), .selecter(), .out(DRB[7:0])); 
	outputleg  pcC(.a(DR[7:4]), .selecter(), .out(DRC[7:0])); 
	outputleg  pcD(.a(DR[3:0]), .selecter(), .out(DRD[7:0])); 
	
	///connect SIMPLEB 
	SIMPLEBtest simple(.clock(clock),.reset(reset),.exInput(exInput),.exOutput(exOutput),.debugIR(IR),.debugDR(DR)); 
	
	endmodule

