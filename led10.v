module led10(
   input [3:0] in,
	output [7:0] out,
	gnd = 0
);

function [7:0] decode;
input [3:0] a;
begin
  case (a)
   0: decode = 8'b0111_1110;
	1: decode = 8'b0011_0000;
	2: decode = 8'b0110_1101;
	3: decode = 8'b0111_1001;
	4: decode = 8'b0011_0011;
	5: decode = 8'b0101_1011;
	6: decode = 8'b0101_1111;
	7: decode = 8'b0111_0010;
	8: decode = 8'b0111_1111;
	9: decode = 8'b0111_1011;
	default: decode = 8'b0100_0000;
  endcase
end
endfunction

assign out = decode(in);
endmodule