module p3 (
	input [15:0] AR, BR,
	input [1:0] op1,       //IR[15:14]
	input [2:0] op2, op3, //IR[13:11],IR[10:8]
	input [3:0] op4, op5, //IR[7:4],IR[3:0]
	output exOutFrag,
	output [15:0] DR,
	output [16:0] out,
	output reg [3:0] code     //conditions code
);

integer s = 3, z = 2, c = 1, v = 0;
integer i;
//reg [16:0] inst,o;
wire [16:0] ar = {AR[15],AR};
wire[16:0] br = {BR[15],BR};
wire[16:0] add = ar + br;
wire[16:0] sub = ar - br;
wire [16:0] inst = out;


////////////////function///////////////////////

function [15:0] andf;  //and
input [15:0] a,b;
integer n = 0;
begin
	for(n=0; n<16; n=n+1)
	begin
		if(a[n] == 1 && b[n] == 1) andf[n] = 1;
		else andf[n] = 0;
 	end
end
endfunction

function [15:0] orf;  //or
input [15:0] a,b;
integer n = 0;
begin
	for(n=0; n<16; n=n+1)
	begin
		if(a[n] == 1 || b[n] == 1) orf[n] = 1;
		else orf[n] = 0;
	end
end
endfunction

function [15:0] xorf;  //xor
input [15:0] a,b;
integer n = 0;
begin
	for(n=0; n<16; n=n+1)
	begin
		if(a[n] == 1 && b[n] == 1 || a[n] == 0 && b[n] == 0) xorf[n] = 0;
		else xorf[n] = 1;
	end
end
endfunction


function [15:0] shift_left_logical;  //sll
input [15:0] r;
input [3:0] d;
integer n = d[0] + d[1]*2 + d[2]*4 + d[2]*8;  //how many shift
integer i;
begin
	for(i=15; i+1>0; i=i-1)
	begin
		if(i>n-1)
	   begin
			shift_left_logical[i] = r[i-n];
		end
	   else
		begin
			shift_left_logical[i] = 0;
		end
	end
end
endfunction

function [15:0] shift_left_rotate;  //slr
input [15:0] r;
input [3:0] d;
integer n = d[0] + d[1]*2 + d[2]*4 + d[2]*8;  //how many shift
integer i;
begin
	for(i=15; i+1>0; i=i-1)
	begin
		if(i>n-1)
		begin
			shift_left_rotate[i] = r[i-n];
		end
	   else
		begin
			shift_left_rotate[i] = r[15-n+1+i];
		end
	end	 
end
endfunction

function [15:0] shift_right_logical;  //srl
input [15:0] r;
input [3:0] d;
integer n = d[0] + d[1]*2 + d[2]*4 + d[2]*8;  //how many shift
integer i;
begin
	for(i=0; i<16; i=i+1)
	begin
		if(i<16-n)
		begin
			shift_right_logical[i] = r[i+n];
		end
	   else
		begin
			shift_right_logical[i] = 0;
		end
	end
end
endfunction

function [15:0] shift_right_arithmetic;  //sra
input [15:0] r;
input [3:0] d;
integer n = d[0] + d[1]*2 + d[2]*4 + d[2]*8;  //how many shift
integer i;
begin
	for(i=0; i<16; i=i+1)
	begin
		if(i<16-n)
		begin
			shift_right_arithmetic[i] = r[i+n];
		end
	   else
		begin
			shift_right_arithmetic[i] = r[15];
		end
	end
end
endfunction

function [16:0] branch;  //branch instruction
input [2:0] cond;
integer judge = 0;
begin
	case (cond)
		3'b000 : if(code[z]) judge = 1;
		3'b001 : if(code[s] ^ code[v]) judge = 1;
		3'b010 : if(code[z] || (code[s] ^ code[v])) judge = 1;
		3'b011 : if(~code[z]) judge = 1;
		default : judge = 0;
	endcase
	if(judge)
	begin
		case(op4[3])
		1 : branch = {9'b11111_1111, op4, op5};
		0: branch = {9'b00000_0000, op4, op5};
		default : branch = 0;
		endcase
	end
	else
	begin
		branch = 17'b00000_0000_0000_0000;
	end 
end
endfunction


////////////////////////////  set output ///////////////////////////

///  set DR ///
function [15:0] calc;
input [1:0] op1;
input [2:0] op2, op3;
input [3:0] op4, op5;
input [15:0] AR, BR;
begin
	if(op1 == 2'b11) begin//calcuration instruction
		if(op4 == 4'b0000)  begin//add  AR:Rd,BR:Rs
			calc = AR + BR;
		end else if(op4 == 4'b0001) begin //sub  AR:Rd,BR:Rs
			calc = AR - BR;
		end else if(op4 == 4'b0010) begin //and  AR:Rd,BR:Rs
			calc[15:0] = andf(AR,BR);
		end else if(op4 == 4'b0011) begin	//or  AR:Rd,BR:Rs
			calc[15:0] = orf(AR,BR);
		end else if(op4 == 4'b0100) begin //xor  AR:Rd,BR:Rs
			calc[15:0] = xorf(AR,BR);
		end else if(op4 == 4'b0110) begin //mov  BR:Rs
			calc = BR;
		end else if(op4 == 4'b1000) begin //sll  AR:Rd
			calc[15:0] = shift_left_logical(AR,op5);
		end else if(op4 == 4'b1001) begin //slr  AR:Rd
			calc[15:0] = shift_left_rotate(AR,op5);
		end else if(op4 == 4'b1010) begin //srl  AR:Rd
			calc[15:0] = shift_right_logical(AR,op5);
		end else if(op4 == 4'b1011) begin //sra  AR:Rd
			calc[15:0] = shift_right_arithmetic(AR,op5);
		end
	end
  
	else if(op1[1] == 0) begin //load or store  AR:Ra,BR:Rb
		calc = BR + {8'b0000_0000,op4,op5};
	end
  
	else if(op1 == 2'b10) begin //load immediate or branch
		case (op2)
			3'b000 : calc = {8'b0000_0000,op4,op5}; //li
			3'b100 : calc = 16'b0000_0000_0000_0001 + 
			                 {op4[3],op4[3],op4[3],op4[3],op4[3],op4[3],op4[3],op4[3],op4,op5}; //B  AR:PC
			3'b111 : calc = branch(op3); //conditional branch
			default : calc = 16'b0000_0000_0000_0000;
		endcase
	end else begin
		calc = 16'b0000_0000_0000_0000;
	end
end
endfunction

/// set out ///
function [16:0] exOutput;
input [1:0] op1;
input [3:0] op4;
input [16:0] br;
begin
	if(op1 == 2'b11)  begin//calcuration instruction
		if(op4 == 4'b1101) begin//out  BR:Rd
			exOutput[16:0] = br;
		end else if(op4 == 4'b1111) begin //hlt
			exOutput[16] = 1;
	   end
	end
end
endfunction

///set exOutFrag ///
function Ofrag;
input [1:0] op1;
input [3:0] op4;
begin
	if(op1 == 2'b11) begin
		if(op4 == 4'b1101) Ofrag = 1;
		else if(op4 == 4'b1111) Ofrag = 1;
		else Ofrag = 0;
	end else begin
		Ofrag = 0;
	end
end
endfunction

/// set code ///
function [3:0] setCode;
input [1:0] op1;
input [3:0] op4, op5;
input [16:0] ar, br;
input[16:0] add;
input[16:0] sub;
begin
	integer N = op5[0] + op5[1]*2 + op5[2]*4 + op5[3]*8;
	if(op1 == 2'b11)  //calcuration instruction
   begin
		if(op4 == 4'b0000)  begin//add  AR:Rd,BR:Rs
			setCode[c] = add[16];
			if(add[15] == 1) begin
				if(ar[15] == 0 && br[15] == 0)  begin //overflow(plus)
					setCode[v] = 1;
					setCode[s] = 0;
					setCode[z] = 0;
				end else if(ar[15] == 1 && br[15] == 1) begin  //overflow(minus)
					setCode[v] = 1;
					setCode[s] = 1;
					setCode[z] = 0;
				end else begin   //minus
					setCode[s] = 1;
					setCode[v] = 0;
					setCode[z] = 0;
				end
			end else if(add == 17'b00000_0000_0000_0000) begin //zero
				setCode[z] = 1;
				setCode[s] = 0;
				setCode[v] = 0;
			end
		end else if(op4 == 4'b0001) begin //sub  AR:Rd,BR:Rs
			setCode[c] = sub[16];
			if(sub[15] == 1 && ar[15] == 0 && br[15] == 1) begin //overflow(plus)
				setCode[v] = 1;
				setCode[s] = 0;
				setCode[v] = 0;
			end else if(sub[15] == 0 && ar[15] == 1 && br[15] == 0) begin //overflow(minus)
				setCode[v] = 1;
				setCode[s] = 1;
				setCode[z] = 0;
			end else if(sub[15] == 1) begin //minus
 				setCode[s] = 1;
				setCode[z] = 0;
				setCode[v] = 0;
			end else if(ar == br) begin //zero
				setCode[z] = 1;
				setCode[s] = 0;
				setCode[v] = 0;
			end
		end else if(op4 == 4'b0010) begin //and  AR:Rd,BR:Rs
			setCode = 4'b0000;
		end else if(op4 == 4'b0011) begin	//or  AR:Rd,BR:Rs
			setCode = 4'b0000;
		end else if(op4 == 4'b0100) begin //xor  AR:Rd,BR:Rs
			setCode = 4'b0000;
		end else if(op4 == 4'b0101) begin //cmp  AR:Rd,BR:Rs
			setCode[c] = 0;
			setCode[v] = 0;
			if(br[15] == 0 && sub[16] == 1) begin //AR < BR
				setCode[s] = 1;
				setCode[z] = 0;
			end else if(sub == 0) begin //AR == BR
				setCode[z] = 1;
				setCode[s] = 0;
			end	
		end else if(op4 == 4'b0110) begin //mov  BR:Rs
			setCode = 4'b0000;
		end else if(op4 == 4'b1000) begin //sll  AR:Rd
		   setCode[v] = 0;
			setCode[s] = 0;
			setCode[z] = 0;
		   if(N == 0) begin
				setCode[c] = 1;
			end else begin
				setCode[c] = ar[15-N+1];
			end
		end else if(op4 == 4'b1001) begin //slr  AR:Rd
		   setCode[v] = 0;
			setCode[s] = 0;
			setCode[z] = 0;
		   if(N == 0) begin
				setCode[c] = 1;
			end else	begin
				setCode[c] = ar[15-N+1];
			end
		end else if(op4 == 4'b1010) begin //srl  AR:Rd
		   setCode[v] = 0;
			setCode[s] = 0;
			setCode[z] = 0;
		   if(N == 0) begin
				setCode[c] = 1;
			end else	begin
				setCode[c] = ar[15-N+1];
			end
		end else if(op4 == 4'b1011) begin //sra  AR:Rd
		   setCode = 4'b0000;
		end else if(op4 == 4'b1100) begin //in
			setCode = 4'b0000;
		end else if(op4 == 4'b1101) begin//out  BR:Rd
			setCode = 4'b0000;
		end else if(op4 == 4'b1111) begin //hlt
			setCode = 4'b0000;
		end
	end
  
	else if(op1[1] == 0) begin //load or store  AR:Ra,BR:Rb
		setCode = 4'b00000;
	end
  
	else if(op1 == 2'b10) begin //load immediate or branch
		setCode = 4'b0000;
	end else begin
		setCode = 4'b0000;
	end
end
endfunction

initial begin
	code <= 4'b0000;
end

always @* begin
	code <= setCode(op1, op4, op5, ar, br, add, sub);
end

assign DR = calc(op1, op2, op3, op4, op5, AR, BR);
assign out = exOutput(op1, op4, br);
assign exOutFrag = Ofrag(op1, op4);

  
endmodule
