module p5(
  input [15:0] DR,MDR,
  input [1:0] op1,
  input [2:0] op2,op3,
  input [3:0] op4,
  output [15:0] out1,out2,       //output to phase P1,P2
  output p5adress1,p5adress2,   
  output [2:0] address           //where to write the data at phase 2
);

wire [36:0] inst;

function [36:0] funcO;  //0~15;out1, 16~31:out2, 32~34:address, 35:p5adress1, 36:p5adress2
input [1:0] op1;
input [2:0] op2, op3;
input [3:0] op4;
input [15:0] DR, MDR;
begin
  if(op1 == 2'b11) begin  //calcuration instruction
    if(op4 == 4'b0101 && op4 == 4'b0111 
	      && op4 == 4'b1101 && op4[3:1] == 3'b111) begin
		funcO[36] = 0;
		funcO[35] = 0;
	 end else if(op4 == 4'b1100) begin
		funcO[31:16] = MDR;
		funcO[34:32] = op3;
		funcO[36] = 1;
	 end else begin
	   funcO[31:16] = DR;
		funcO[34:32] = op3;
		funcO[35] = 0;
		funcO[36] = 1;
	 end
  end
  
  else if(op1 == 2'b00 || op1 == 2'b01) begin  //load or store
		funcO[34:32] = op2;
      funcO[31:16] = MDR;
	   funcO[35] = 0;
	   funcO[36] = 1;
  end
  
  else if({op1,op2} == 5'b10000) begin //load immediate 
		funcO[34:32] = op3;
      funcO[31:16] = DR;
	   funcO[35] = 0;
	   funcO[36] = 1;
  end
  
  else if({op1,op2} == 5'b10100 || {op1,op2} == 5'b10111) begin  //branch
      funcO[15:0] = DR;
		funcO[35] = 1;
	   funcO[36] = 0;
  end
end
endfunction

assign inst = funcO(op1,op2,op3,op4,DR,MDR);
assign out1 = inst[15:0];
assign out2 = inst[31:16];
assign address = inst[34:32];
assign p5adress1 = inst[35];
assign p5adress2 = inst[36];
endmodule
