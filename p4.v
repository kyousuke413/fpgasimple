module p4(
	input [15:0] in,    //from external input
					 data_path,  //from data_path(from RAM)
					 DR_I,
	input [1:0] op1,
	input [3:0] op4,
	output [15:0] MDR, DR_O
);

reg [15:0] out;
	
always @*
begin
	if(op1[1] == 0)    //load or store
	begin
		out <= data_path;
   end
	else if(op1 == 2'b11 && op4 == 4'b1100)  //in
   begin
		out <= in;
   end else begin
		out <= 0;
	end
end

assign MDR = out;
assign DR_O = DR_I;

endmodule