module chattering(
  input clk,reset,in,
  output reg out
  );
  reg [15:0] count;
  
  always @(posedge clk or negedge reset)begin
    if(!reset) count <= 0;
	 else count <= count + 16'b0000_0000_0000_0001;
  end
  
  always @(posedge clk)begin 
    if(count == 0) out <= in;
  end
  
endmodule