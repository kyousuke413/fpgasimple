module p1(
input fetch, start, set_p5address,reset,
input [15:0] p5,
input [15:0] data_path,
output reg [15:0] IR,
output reg [15:0] pc//for address path
);

always @* begin
IR <= data_path[15:0]; 
end

initial begin
pc = 16'b0000_0000_0000_0000;
end

function [15:0] tranState;
input [15:0] pc;
input reset;
input p1wren;
begin
	if(p1wren == 1)begin
	case(reset)
	1 : tranState = 16'b0000_0000_0000_0000;
	0 : tranState = pc + p5;
	default : tranState = pc + 16'b0000_0000_0000_0001;
	endcase
	end else begin
	case (reset)
	1 : tranState = 16'b0000_0000_0000_0000;
	0 : tranState = pc + 16'b0000_0000_0000_0001;
	default : tranState = pc + 16'b0000_0000_0000_0001;
	endcase
	end
end
endfunction

always @(posedge start) begin
	pc <= tranState(pc,reset,set_p5address);
end

endmodule
